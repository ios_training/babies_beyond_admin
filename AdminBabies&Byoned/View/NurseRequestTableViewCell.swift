//
//  NurseRequestTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseRequestTableViewCell: UITableViewCell {
    @IBOutlet weak var nurseRequestDate: UILabel!
    @IBOutlet weak var nurseRequestName: UILabel!
    @IBOutlet weak var nurseRequestImage: UIImageView!
    @IBOutlet weak var nurseViewDetails: UIButton!
    var delegate:NurseRequestDelegate?
    var request:NurseStaff_request?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.nurseViewDetails.layer.cornerRadius = 3.0
        nurseRequestImage.layer.cornerRadius = nurseRequestImage.frame.size.height/2
        nurseRequestImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    @IBAction func showViewDetails(_ sender: Any) {
        self.delegate?.ViewNurseRequestDetails(btnTag: nurseViewDetails.tag,passedID: (request?.id)!)
    }
}

protocol NurseRequestDelegate{
    func ViewNurseRequestDetails(btnTag:Int,passedID:String)
}
