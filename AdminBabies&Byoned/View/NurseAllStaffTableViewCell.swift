//
//  NurseAllStaffTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseAllStaffTableViewCell: UITableViewCell {

    @IBOutlet weak var allStaffImage: UIImageView!
    @IBOutlet weak var allStaffName: UILabel!
    @IBOutlet weak var allStaffReserved: UILabel!
    @IBOutlet weak var allStaffViewDetails: UIButton!
    
    var delegate:AllStaffDelegate?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        allStaffViewDetails.layer.cornerRadius = 3.0
        allStaffImage.layer.cornerRadius = allStaffImage.frame.size.height/2
        allStaffImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func allStaffViewDetails(_ sender: Any) {
        self.delegate?.showAllStaffNurseDetails(btnTag: allStaffViewDetails.tag)
        }
       
    
    
}

protocol AllStaffDelegate{
    func showAllStaffNurseDetails(btnTag: Int)
}
