//
//  MidwifeTimeSlotTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/28/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class MidwifeTimeSlotTableViewCell: UITableViewCell {
    @IBOutlet weak var startTimeLabel:UILabel!
    @IBOutlet weak var endTimeLabel:UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
