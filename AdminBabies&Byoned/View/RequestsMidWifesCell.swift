//
//  RequestsMidWifesCell.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/28/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class RequestsMidWifesCell: UITableViewCell {

    @IBOutlet weak var RequestMidWifesName: UILabel!
    @IBOutlet weak var RequestMidWifesImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        RequestMidWifesImage.layer.cornerRadius = RequestMidWifesImage.frame.size.height/2
        RequestMidWifesImage.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }

}
