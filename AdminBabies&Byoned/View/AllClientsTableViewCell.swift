//
//  AllClientsTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 3/14/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class AllClientsTableViewCell: UITableViewCell {

    @IBOutlet weak var AllClientsImage: UIImageView!
    @IBOutlet weak var AllClientsName: UILabel!
    @IBOutlet weak var AllClientsDetailsButton: UIButton!
    
    var delegate:AllClientsDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.AllClientsDetailsButton.layer.cornerRadius = 3.0
        AllClientsImage.layer.cornerRadius = AllClientsImage.frame.size.height/2
        AllClientsImage.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func allClientsViewDetails(_ sender: Any) {
        self.delegate?.showAllClientsDetails(btnTag: AllClientsDetailsButton.tag)
    }
    
    
}


protocol AllClientsDelegate{
    func showAllClientsDetails(btnTag:Int)
}

