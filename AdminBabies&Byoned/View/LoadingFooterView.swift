//
//  LoadingFooterView.swift
//  AdminBabies&Byoned
//
//  Created by NTAM on 4/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class LoadingFooterView: UIView {

    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
  */
    override func draw(_ rect: CGRect) {
        activityIndicatorView.startAnimating()

    }
 


}
