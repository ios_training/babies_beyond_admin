//
//  AllMidWifesTableViewCell.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/27/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class AllMidWifesTableViewCell: UITableViewCell {

    @IBOutlet weak var allStaffMidwifesImage: UIImageView!
    @IBOutlet weak var allStaffMidwifesName: UILabel!
  
    
    var delegate:AllStaffMidWifesDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        allStaffMidwifesImage.layer.cornerRadius = allStaffMidwifesImage.frame.size.height/2
        allStaffMidwifesImage.clipsToBounds = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

protocol AllStaffMidWifesDelegate{
    func showAllStaffMidWifesDetails(btnTag: Int)
}
