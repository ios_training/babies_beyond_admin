//
//  LoadingView.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    lazy var activityIndicator : UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
        aiv.startAnimating()
        aiv.hidesWhenStopped = true
        aiv.translatesAutoresizingMaskIntoConstraints = false
        return aiv
    }()
    
    var loadingMessage : UILabel = {
        let messageLabel = UILabel()
        messageLabel.text = "Loading..."
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont.systemFont(ofSize: 12)
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        return messageLabel
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.insertViews()
    }
    
    func insertViews() {
        addSubview(activityIndicator)
        activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        activityIndicator.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
        
        addSubview(loadingMessage)
        // message autoLayout constraints
        loadingMessage.topAnchor.constraint(equalTo: activityIndicator.bottomAnchor, constant: -5).isActive = true
        loadingMessage.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        loadingMessage.widthAnchor.constraint(equalToConstant: 100).isActive = true
        loadingMessage.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
