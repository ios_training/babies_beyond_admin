//
//  Extensionview.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/1/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    func backToLoginScreen(){
        var stoaryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = stoaryboard.instantiateViewController(withIdentifier: "SignInViewController") as! SignInViewController
        self.present(vc, animated: true, completion: nil)
    }
}
