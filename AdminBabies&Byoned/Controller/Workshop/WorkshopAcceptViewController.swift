//
//  WorkshopAcceptViewController.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/2/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import PKHUD

class WorkshopAcceptViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var workshopName: UILabel!
    @IBOutlet weak var workshopDateFrom: UILabel!
    @IBOutlet weak var workshopDateTo: UILabel!
    @IBOutlet weak var workshopTimeFrom: UILabel!
    @IBOutlet weak var workshopTimeTo: UILabel!
    @IBOutlet weak var workshopLocation: UILabel!
    @IBOutlet weak var workshopSpeakerName: UILabel!
    @IBOutlet weak var workshopPrice: UILabel!
    @IBOutlet weak var workshopBio: UITextView!
    @IBOutlet weak var createInvoiceTapped: EMSpinnerButton!
    
    // MARK:- Variables
    var userID :String!
    var userWorkshopID :String!
    var workshopRequestsViewController : WorkshopRequestViewController?
    var adminworkshopRequest : WorkshopData?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createInvoiceTapped.cornerRadius = 5.0
        self.createInvoiceTapped.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        // Do any additional setup after loading the view.
        self.title = "Workshop Requests"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        let button1 = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: .plain, target: self, action: #selector((buttonBackWorkshop)))
        self.navigationItem.leftBarButtonItem  = button1
        
        if let obj = adminworkshopRequest{
            workshopName.text = obj.name
            if let startDate = obj.start_date{
                workshopDateFrom.text = Helper.dateConverterWithFormat(dateString : startDate, isDate: true)
            }
            else{
                workshopDateFrom.text = ""
            }
            if let startTime = obj.start_date{
                workshopTimeFrom.text = Helper.dateConverterWithFormat(dateString : startTime, isDate: false)
            }
                
            else{
                workshopTimeFrom.text = ""
            }
            if let endDate = obj.end_date{
                workshopDateTo.text = Helper.dateConverterWithFormat(dateString : endDate, isDate: true)
            }
            else{
                workshopDateTo.text = ""
            }
            if let endTime = obj.end_date{
                workshopTimeTo.text = Helper.dateConverterWithFormat(dateString : endTime, isDate: false)
            }
            else{
                workshopTimeTo.text = ""
            }
            workshopLocation.text = obj.location
            workshopSpeakerName.text = obj.speaker_name
            workshopBio.text = obj.speaker_bio
            workshopPrice.text = obj.price
            userID = obj.user_id
            userWorkshopID = obj.user_workshop_id
        }

    }

    @objc func buttonBackWorkshop(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func workshopCreateInvoice(_ sender: Any) {
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        
        guard let  userID = userID  else {
            return
        }
        
        guard let userWorkshopID = userWorkshopID else {
            return
        }
        
        
        if InternetConnection.connected(){
            self.createInvoiceTapped.animate(animation: .collapse)
            assignInvoiceToWorkshop(adminID: adminID, userID: userID, userWorkshopID: userWorkshopID)
            

        }
            
        else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }
    
    func assignInvoiceToWorkshop (adminID:Int,userID:String,userWorkshopID:String){
        let parameters = ["admin_id":adminID,"user_id":userID,"user_workshop_id": userWorkshopID] as [String:Any]
        AdminServices.createWorkshopInvoiceService(Constants.WorkShopInvoice, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.createInvoiceTapped.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? WorkshopCreateInvoiceResponse {
                    if result.status!{
                        self.workshopRequestsViewController?.showWorkshopRequests()
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.createInvoiceTapped.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    return
                }
            }
        })
    }
   
}
