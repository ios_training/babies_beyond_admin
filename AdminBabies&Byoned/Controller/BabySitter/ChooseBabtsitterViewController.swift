//
//  ChooseBabtsitterViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/19/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class ChooseBabtsitterViewController: UIViewController,UINavigationControllerDelegate{
    
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var babySitterSubmitButton: UIButton!
    
    // MARK:- Variables
    var babysitterAssigmentVC: BabySitterAssigmentViewController!
    var navBar:UINavigationController?
    var availableStaffRequests = [GetAvailableStaff]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    var serviceTypeID:String!
    var serviceStartDate:String!
    var serviceEndDate:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        babySitterSubmitButton.layer.cornerRadius = 5.0
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        self.title = "Choose a babysitter"
        observeChanges()
    }
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    //func if internet connection lost suddenly and to update data if internet come sudden
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    override func viewWillAppear(_ animated: Bool) {
        setUpView()
    }
    func updateUI(){
        if AccountManager.shared().availableStaff?.data == nil{
        if reachability.currentReachabilityStatus == .notReachable {
            connectionBtn?.isHidden = true
        }else{
            setUpView()
        }
        }
    }
    
    //file for reload data
    fileprivate func setUpView() {
                if let staffRequests = AccountManager.shared().availableStaff?.data{
        
                    loadingView?.isHidden = true
                    connectionBtn?.isHidden = true
                    tableView.isHidden = false
                    tableView.reloadData()
                }
        else if InternetConnection.connected(){
                    loadingView?.isHidden = false
                    connectionBtn?.isHidden = true
                    tableView.isHidden = true
            guard let adminID = AccountManager.shared().userData?.id else{
                return
            }
            
            guard let  serviceTypeID = serviceTypeID  else {
                return
            }
            
            guard let serviceStartDate = serviceStartDate else {
                return
            }
            guard let serviceEndDate = serviceEndDate else {
                return
            }
            getAvailableStaffAdmin(admin_id: adminID, service_type_id: serviceTypeID, service_start_date: serviceStartDate, service_end_date: serviceEndDate)
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    @IBAction func babySitterSubmitButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    func getAvailableStaffAdmin(admin_id : Int,service_type_id:String,service_start_date:String,service_end_date:String)
    {
        let parameters = ["admin_id":admin_id,"service_type_id":"2","service_start_date": serviceStartDate,"service_end_date":serviceEndDate] as [String:Any]
        self.loadingView?.isHidden = false
        AdminServices.getAvailableStaff(Constants.AvailableStaff, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.loadingView?.isHidden = true
                self.connectionBtn?.isHidden = false
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? GetAvailableStaffResponse {
                    // response
                    if let result = response as? GetAvailableStaffResponse {
                        if let staffRequest = result.data{
                            if staffRequest.count > 0 {
                                self.availableStaffRequests.removeAll()
                                self.availableStaffRequests = staffRequest
                                self.loadingView?.isHidden = true
                                self.connectionBtn?.isHidden = true
                                self.tableView?.isHidden = false
                                self.tableView.reloadData()
                            }else{
                                self.connectionBtn?.isHidden = false
                                self.connectionBtn?.isUserInteractionEnabled = false
                                self.loadingView?.isHidden = true
                                self.tableView.isHidden = true
                                self.babySitterSubmitButton.isHidden = true
                                self.connectionBtn?.setTitle(Constants.NoStaffAvailable, for: .normal)
                            }

                        }
                    }
                    else{
                    }
                }else{
                    return
                }
                
                
            }
        })
        
        
        
    }
    
}

extension ChooseBabtsitterViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableStaffRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = availableStaffRequests[indexPath.row].name
        tableView.tableFooterView = UIView()
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for row in 0...tableView.numberOfRows(inSection: 0) {
            if row == indexPath.row {continue}
            tableView.deselectRow(at: IndexPath(row: row, section: 0), animated: true)
        }
        
        if let cell = tableView.cellForRow(at: indexPath) {
            cell.accessoryType = .checkmark
            babysitterAssigmentVC.selectedBabySitterName = availableStaffRequests[indexPath.row].name
            babysitterAssigmentVC.selectedBabySitterID = availableStaffRequests[indexPath.row].id
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = .none
    }
}

