//
//  BabySitterAssigmentViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import PKHUD
import STPopup

class BabySitterAssigmentViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var createInvoiceLoading: EMSpinnerButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var startDateLbl: UILabel!
    @IBOutlet weak var endDateLbl: UILabel!
    @IBOutlet weak var chooseBabySitterName: UIButton!
    @IBOutlet weak var banySitterButtonName: UIButton!
    
    // MARK:- Variables
    var chooseBabySitter:ChooseBabtsitterViewController?
    var babysitterRequestsViewController : BabySitterRequestViewController?
    var staffID:String?
    var serviceID:String?
    var serviceTypeName:String?
    var serviceUserID:String?
    var serviceTypeID:String!
    var serviceStartDate:String!
    var serviceEndDate:String!
    var babysitterRequest : NurseStaff_request?
    var availableStaffRequests = [GetAvailableStaff]()
    var availableStaff : GetAvailableStaff?
    var selectedBabySitterName:String?
    var selectedBabySitterID:String?
    var staffRequest:NurseStaff_request?
    var passedID:String?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createInvoiceLoading.cornerRadius = 5.0
        self.createInvoiceLoading.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        // Do any additional setup after loading the view.
        self.title = "Babysitter Requests"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        let button1 = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: .plain, target: self, action: #selector((buttonBackMidwife)))
        self.navigationItem.leftBarButtonItem  = button1
        setupView()
    }

    override func viewWillAppear(_ animated: Bool) {
        if selectedBabySitterName == "" || selectedBabySitterName == nil{
            chooseBabySitterName.setTitle("Any", for: .normal)
        }else{
            chooseBabySitterName.setTitle(selectedBabySitterName, for: .normal)
        }
    }
    func setupView()  {
        if let obj = staffRequest{
            nameLbl.text = obj.user_name
            locationLbl.text = obj.location
            startDateLbl.text = Helper.fullDateConverter(dateString: obj.start_date!)
            endDateLbl.text = Helper.fullDateConverter(dateString: obj.end_date!)
            staffID = obj.staff_id
            serviceID = obj.id
            serviceTypeName = obj.service_type_name
            serviceUserID = obj.user_id
            serviceTypeID = obj.service_type_id
            serviceStartDate = obj.start_date
            serviceEndDate = obj.end_date
            
        }
    }
    
    @objc func buttonBackMidwife(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func babySitterChooseNurse(_ sender: Any) {
        
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        chooseBabySitter?.getAvailableStaffAdmin(admin_id: adminID, service_type_id: serviceTypeID, service_start_date: serviceStartDate, service_end_date: serviceEndDate)
        let destination = self.storyboard!.instantiateViewController(withIdentifier: "ChooseBabtsitterViewController") as? ChooseBabtsitterViewController
        destination?.serviceTypeID = serviceTypeID
        destination?.serviceStartDate = serviceStartDate
        destination?.serviceEndDate = serviceEndDate
        destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
        destination?.babysitterAssigmentVC = self
        let popupController = STPopupController.init(rootViewController: destination!)
        popupController.containerView.layer.cornerRadius = 5.0
        popupController.present(in: self)
    }
    
    @objc func buttonBackNurse(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func midwifeCreateInvoice(_ sender: Any) {
       
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        guard let staffIDs = selectedBabySitterID else{
            return
        }
        guard let serviceID = serviceID else{
            return
        }
        guard let serviceTypeName = serviceTypeName else{
            return
        }
        guard let serviceUserID = serviceUserID else{
            return
        }
        if InternetConnection.connected() {
            self.createInvoiceLoading.animate(animation: .collapse)
           assignService(admin_id: adminID, staff_id: staffIDs, service_id: serviceID, service_type_name: serviceTypeName, user_id: serviceUserID)
        }else{
            // show alert
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
        

    }

    func assignService (admin_id : Int,staff_id:String,service_id:String,service_type_name:String,user_id:String){
        
       let parameters = ["admin_id":admin_id,"staff_id":selectedBabySitterID!, "service_id":service_id,"service_type_name":service_type_name, "user_id":user_id ] as [String:Any]
        AdminServices.assignServiceToStaff(Constants.AssignServiceApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.createInvoiceLoading.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? AssignServiceModel {
                    if result.status!{
                        self.babysitterRequestsViewController?.showAllRequests()
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        self.createInvoiceLoading.animate(animation: .expand)
                        Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }
                else{

                    return
                }

            }
        })
    }
}
