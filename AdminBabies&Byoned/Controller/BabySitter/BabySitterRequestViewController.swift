//
//  BabySitterRequestViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import STPopup
import ISHPullUp
import SDWebImage
import ESPullToRefresh
import Toast_Swift

class BabySitterRequestViewController: UIViewController,MidwifeRequestDelegate {

    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Variables
    var navBar:UINavigationController?
    var staffRequest = [NurseStaff_request]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchUpInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        let nib = UINib.init(nibName: "MidwifeRequestTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "midwifeRequestTableViewCell")
        observeChanges()
        
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.showAllRequests()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.tabBarController?.view.makeToast("Check your Internet Connection!")
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }

    }
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
    
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(babysitterChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Babysitter.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Babysitter.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    
    @objc func babysitterChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        switch action {
        case NotificationAction.CancelAction.rawValue:
            guard let babysitterId = notification.userInfo?[Constants.Id] as? String else {
                return
            }

            if let i = AccountManager.shared().babySitterRequestRespone?.data?.staff_request?.index(where:{$0.id == babysitterId}) {
                AccountManager.shared().babySitterRequestRespone?.data?.staff_request?.remove(at: i)
                staffRequest.remove(at: i)
            }
            self.tableView.reloadData()
            break
        case NotificationAction.AddAction.rawValue:
            if let babysitterObj = notification.userInfo?[Constants.serviceObj] as? [String:Any] {
                let jsonData = try? JSONSerialization.data(withJSONObject: babysitterObj, options: [])
                var babysitterData: NurseStaff_request?
                if let data = jsonData {
                    babysitterData = try? JSONDecoder().decode(NurseStaff_request.self, from: data)
                }
                AccountManager.shared().babySitterRequestRespone?.data?.staff_request?.append(babysitterData!)
                staffRequest.append(babysitterData!)
                self.tableView.reloadData()
            }
            break
        default: break
        }
    }

    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        // Hide the navigation bar for current view controller
        self.navigationController?.isNavigationBarHidden = true
        setUpView()
    }
    func updateUI() {
        if AccountManager.shared().babySitterRequestRespone == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    fileprivate func setUpView() {
        if let services = AccountManager.shared().babySitterRequestRespone?.data?.staff_request{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            showAllRequests()
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    func  ViewMidwifeRequestDetails(btnTag: Int,passedID:String) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "BabySitterAssigmentViewController") as! BabySitterAssigmentViewController
        vc.passedID = passedID
        vc.staffRequest = staffRequest[btnTag]
        vc.babysitterRequestsViewController = self
        self.navBar?.pushViewController(vc, animated: true)
    }
    
    func showAllRequests(){
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        let serviceTypeId = Constants.BabysitterTypeID
        let parameters = ["admin_id":adminID,"service_type_id":serviceTypeId] as [String:Any]
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        AdminServices.allAdminNurseRequests(Constants.AllAdminNurseRequestApi, params: parameters, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    let result = UIScreen.main.bounds.size
                    if result.height <= 568{
                        self.connectionBtn?.setTitle(Constants.FailureAlertiPhone5, for: .normal)
                    }else{
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                }else{
                    self.tableView.es.stopPullToRefresh()
                    self.tabBarController?.view.makeToast("Check your Internet Connection!")
                }
            }else{
                // response
                if let result =  response as? NurseRequestResponse {
                    AccountManager.shared().babySitterRequestRespone = result
                    self.staffRequest.removeAll()
                    self.staffRequest = (AccountManager.shared().babySitterRequestRespone?.data?.staff_request)!
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                   // }
                }else{
                    return
                }
            }
        })
    }

    
}
extension BabySitterRequestViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return staffRequest.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "midwifeRequestTableViewCell", for: indexPath)  as! MidwifeRequestTableViewCell
        let obj = staffRequest[indexPath.row]
        cell.timeDateLbl.text = "\(Helper.fullDateConverter(dateString: obj.start_date!)!) to \(Helper.fullDateConverter(dateString: obj.end_date!)!)"
        cell.nameLbl.text = obj.user_name
        cell.viewDetailsBtn.tag = indexPath.row
        cell.img.sd_setImage(with: URL(string: obj.user_photo!), placeholderImage: UIImage(named: "Personimage"))
        cell.delegate = self
        cell.babySitterrequest = obj
        return cell
    }
}
