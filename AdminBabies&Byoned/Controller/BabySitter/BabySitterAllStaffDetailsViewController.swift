//
//  BabySitterAllStaffDetailsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class BabySitterAllStaffDetailsViewController: UIViewController {
     var BabySitterallStaffData : All_staff?
    
    // MARK:- Outlets
    @IBOutlet weak var BabySitterDetailsName: UILabel!
    @IBOutlet weak var BabySitterDetailsEmail: UILabel!
    @IBOutlet weak var BabySitterDetailsPhone: UILabel!
    @IBOutlet weak var BabySitterDetailsBirthDay: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "BabySitter Information"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        let button1 = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: .plain, target: self, action: #selector((buttonBackNurse)))
        self.navigationItem.leftBarButtonItem  = button1
        
        if let obj = BabySitterallStaffData{
            BabySitterDetailsName.text = obj.name
            BabySitterDetailsEmail.text = obj.email
            BabySitterDetailsPhone.text = obj.phone
            if let birth = obj.birthday{
                BabySitterDetailsBirthDay.text = Helper.dateConverterWithFormat(dateString : birth, isDate: true)
            }
            else{
                BabySitterDetailsBirthDay.text = ""
            }
        }
    }
    
    @objc func buttonBackNurse(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }

}
