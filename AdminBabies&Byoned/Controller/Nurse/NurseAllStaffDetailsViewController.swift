//
//  NurseAllStaffDetailsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/22/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class NurseAllStaffDetailsViewController: UIViewController {
    
    
    // MARK:- Outlets
    @IBOutlet weak var nurseDetailsName: UILabel!
    @IBOutlet weak var nursedetailsEmail: UILabel!
    @IBOutlet weak var nurseDetailsPhone: UILabel!
    @IBOutlet weak var nurseDetailsBirthDay: UILabel!
    
    // MARK:- Variables
    var allStaffData : All_staff?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.title = "Nurse Information"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        let button1 = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: .plain, target: self, action: #selector((buttonBackNurse)))
        self.navigationItem.leftBarButtonItem  = button1
        
        if let obj = allStaffData{
           nurseDetailsName.text = obj.name
            nursedetailsEmail.text = obj.email
            nurseDetailsPhone.text = obj.phone
            if let birth = obj.birthday{
                nurseDetailsBirthDay.text = Helper.dateConverterWithFormat(dateString : birth, isDate: true)
            }
            else{
                nurseDetailsBirthDay.text = ""
            }
        }
    }

    @objc func buttonBackNurse(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
