
//
//  NurseRequestViewController.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/1/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import ISHPullUp
import Reachability
import ESPullToRefresh
import Toast_Swift

class NurseRequestViewController: UIViewController ,NurseRequestDelegate {
    
    // MARK:- Outlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK:- Variables
    var navBar:UINavigationController?
    let cellIdentifier = "NurseRequestTableViewCell"
    var adminnurserequests = [NurseStaff_request]()
    var loadingView:LoadingView?
    var connectionBtn : ConnectionButton?
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    public var type: ESRefreshType = ESRefreshType.wechat
    var isRefresh : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        let centerY = self.view.bounds.size.height/2 - 100
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        connectionBtn?.addTarget(self, action: #selector(connectionBtnTapped), for: .touchDragInside)
        self.view.addSubview(connectionBtn!)
        
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true
        // Do any additional setup after loading the view.
        let nib = UINib.init(nibName: "NurseRequestTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier:cellIdentifier)
        observeChanges()
        // Header like WeChat
        let header = WeChatTableHeaderView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.bounds.size.width, height: 0)))
        self.tableView.tableHeaderView = header
        
        let _ = self.tableView.es.addPullToRefresh(animator: WCRefreshHeaderAnimator.init(frame: CGRect.zero)) {
            [weak self] in
            if InternetConnection.connected(){
                DispatchQueue.main.asyncAfter(deadline: .now() + 3.0) {
                    self?.isRefresh = true
                    self?.showAllNurseRequests()
                }
            }else{
                DispatchQueue.main.asyncAfter(deadline: .now()) {
                    self?.tabBarController?.view.makeToast("Check your Internet Connection!")
                    self?.tableView.es.stopPullToRefresh()
                }
            }
        }
    }
    
    @objc func connectionBtnTapped() {
        updateUI()
    }
    
    
    
    //func if internet connection lost suddenly and to update data if internet come sudden
    @objc func reachabilityChanged() {
        connectionBtn?.isHidden = true
        updateUI()
    }
   
    private func observeChanges() {
        NotificationCenter.default.addObserver(self, selector: #selector(nurseChanged(_:)), name: NSNotification.Name(rawValue: NotificationType.Nurse.rawValue), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reachabilityChanged), name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
        reachability.startNotifier()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: NotificationType.Nurse.rawValue), object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: FFReachabilityChangedNotification), object: nil)
    }
    
    
    @objc func nurseChanged(_ notification: NSNotification) {
        guard let action = notification.userInfo?[Constants.Action] as? String else{
            return
        }
        switch action {
        case NotificationAction.CancelAction.rawValue:
            guard let nurseId = notification.userInfo?[Constants.Id] as? String else {
                return
            }

            if let i = AccountManager.shared().nurseRequestRespone?.data?.staff_request?.index(where:{$0.id == nurseId}) {
                AccountManager.shared().nurseRequestRespone?.data?.staff_request?.remove(at: i)
                adminnurserequests.remove(at: i)
            }
            self.tableView.reloadData()
            break
        case NotificationAction.AddAction.rawValue:
            if let nurseObj = notification.userInfo?[Constants.serviceObj] as? [String:Any] {
                let jsonData = try? JSONSerialization.data(withJSONObject: nurseObj, options: [])
                var nurseRequestData: NurseStaff_request?
                if let data = jsonData {
                    nurseRequestData = try? JSONDecoder().decode(NurseStaff_request.self, from: data)
                }
                AccountManager.shared().nurseRequestRespone?.data?.staff_request?.append(nurseRequestData!)

                adminnurserequests.append(nurseRequestData!)
                self.tableView.reloadData()
            }
            break
        default: break
        }
    }
    

    
    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }
    override func viewWillAppear(_ animated: Bool) {
        UIApplication.shared.statusBarStyle = .lightContent
        self.navigationController?.isNavigationBarHidden = true
        setUpView()
    }
    func updateUI(){
        if AccountManager.shared().nurseRequestRespone == nil{
            if reachability.currentReachabilityStatus == .notReachable {
                connectionBtn?.isHidden = false
            }else{
                setUpView()
            }
        }
    }
    
    //file for reload data
    fileprivate func setUpView() {
        if let staffRequests = AccountManager.shared().nurseRequestRespone?.data?.staff_request{
            loadingView?.isHidden = true
            connectionBtn?.isHidden = true
            tableView.isHidden = false
            tableView.reloadData()
        }else if InternetConnection.connected(){
            loadingView?.isHidden = false
            connectionBtn?.isHidden = true
            tableView.isHidden = true
            showAllNurseRequests()
            
        }
        else if reachability.currentReachabilityStatus == .notReachable{
            connectionBtn?.isHidden = false
            connectionBtn?.setTitle("Check your Internet Connection!", for: .normal)
        }
    }
    
    func  ViewNurseRequestDetails(btnTag: Int,passedID:String) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "NurseAssigmentViewController") as! NurseAssigmentViewController
        vc.passedID = passedID
        vc.nurseRequest = adminnurserequests[btnTag]
        vc.nurseRequestsViewController = self
        self.navBar?.pushViewController(vc, animated: true)
        
    }
    
    
    func showAllNurseRequests(){
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        let serviceTypeId = Constants.NurseTypeID
        let parameters = ["admin_id":adminID,"service_type_id":serviceTypeId] as [String:Any]
        if isRefresh == false {
            self.loadingView?.isHidden = false
        }
        AdminServices.allAdminNurseRequests(Constants.AllAdminNurseRequestApi, params: parameters, completion: { (response, error) in
            if self.isRefresh == false {
                self.loadingView?.isHidden = true
            }
            if let _ = error {
                if self.isRefresh == false {
                    // show alert with err message
                    self.connectionBtn?.isHidden = false
                    let result = UIScreen.main.bounds.size
                    if result.height <= 568{
                        self.connectionBtn?.setTitle(Constants.FailureAlertiPhone5, for: .normal)
                    }else{
                        self.connectionBtn?.setTitle(Constants.FailureAlert, for: .normal)
                    }
                }else{
                    self.tableView.es.stopPullToRefresh()
                    self.tabBarController?.view.makeToast("Check your Internet Connection!")
                }
            }else{
                // response
                if let result =  response as? NurseRequestResponse {
                    AccountManager.shared().nurseRequestRespone = result
                    self.adminnurserequests.removeAll()
                    self.adminnurserequests = (AccountManager.shared().nurseRequestRespone?.data?.staff_request)!
                    self.connectionBtn?.isHidden = true
                    self.tableView?.isHidden = false
                    self.isRefresh = false
                    self.tableView.reloadData()
                    self.tableView.es.stopPullToRefresh()
                }else{
                    return
                }
            }
        })
    }
}
extension NurseRequestViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adminnurserequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)  as! NurseRequestTableViewCell
        let obj = adminnurserequests[indexPath.row]
        //use tag for button of view details to take data to Assigment screen
        cell.nurseViewDetails.tag = indexPath.row
        //get table view data from webservice
        cell.nurseRequestDate.text =  "\(Helper.fullDateConverter(dateString: obj.start_date!)!) to \(Helper.fullDateConverter(dateString: obj.end_date!)!)"
        if let name = obj.user_name{
            cell.nurseRequestName.text = name
        }

        if let image = obj.user_photo{
             cell.nurseRequestImage.sd_setImage(with: URL(string: obj.user_photo!), placeholderImage: UIImage(named: "Personimage"))
            
        }

        cell.delegate = self
        cell.request = obj
        return cell
    }
    
    

}
