//
//  NurseAssigmentViewController.swift
//  AdminBabies&Byoned
//
//  Created by esam ahmed eisa on 1/1/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import STPopup
import PKHUD


class NurseAssigmentViewController: UIViewController,UINavigationControllerDelegate {

    // MARK:- Outlets
    @IBOutlet weak var assigmentclientname: UILabel!
    @IBOutlet weak var assigmentlocation: UILabel!
    @IBOutlet weak var assigmentdate: UILabel!
    @IBOutlet weak var assigmenttime: UILabel!
    @IBOutlet weak var chooseNurseNameBtn: UIButton!
    @IBOutlet weak var createInvoiceTapped: EMSpinnerButton!
    @IBOutlet weak var chooseNurseDefault: UIButton!
    
    // MARK:- Variables
    var nurseRequest : NurseStaff_request?
    var assignService :AssignServiceModel?
    var assigmentnavBar:UINavigationController?
    var availableStaffRequests = [GetAvailableStaff]()
    var availableStaff : GetAvailableStaff?
    var selectedNurseName:String?
    var selectedNurseID:String?
    var loadingView:LoadingView?
    var chooseNurse:ChooseNurseViewController?
    var nurseRequestsViewController : NurseRequestViewController?
    var staffID:String!
    var serviceID:String!
    var serviceTypeName:String!
    var userId:String!
    var serviceTypeID:String!
    var serviceStartDate:String!
    var serviceEndDate:String!
    var passedID:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.createInvoiceTapped.cornerRadius = 5.0
        self.createInvoiceTapped.backgroundColor = UIColor(red: 226/255, green: 202/255, blue: 193/255, alpha: 1.0)
        self.title = "Nurse Requests"
        let attributes = [NSAttributedStringKey.font : UIFont(name: "MuseoSans-300", size: 20)!, NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = attributes
        self.navigationItem.leftBarButtonItem = nil
        self.navigationItem.hidesBackButton = true
        let button1 = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: .plain, target: self, action: #selector((buttonBackNurse)))
        self.navigationItem.leftBarButtonItem  = button1
        //assign Data to nurse assigment
        if let obj = nurseRequest{
            assigmentclientname.text = obj.user_name
            assigmentlocation.text = obj.location
            assigmentdate.text = Helper.fullDateConverter(dateString: obj.start_date!)
            assigmenttime.text = Helper.fullDateConverter(dateString: obj.end_date!)
            staffID = obj.staff_id
            serviceID = obj.id
            serviceTypeName = obj.service_type_name
            userId = obj.user_id
            serviceTypeID = obj.service_type_id
            serviceStartDate = obj.start_date
            serviceEndDate = obj.end_date
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if selectedNurseName == "" || selectedNurseName == nil{
            chooseNurseDefault.setTitle("Any", for: .normal)
        }else{
            chooseNurseDefault.setTitle(selectedNurseName, for: .normal)
        }
    }
    
    @IBAction func chooseNurseAction(_ sender: Any) {

        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }

            chooseNurse?.getAvailableStaffAdmin(admin_id: adminID, service_type_id: serviceTypeID, service_start_date: serviceStartDate, service_end_date: serviceEndDate)
            let destination = self.storyboard!.instantiateViewController(withIdentifier: "ChooseNurseViewController") as? ChooseNurseViewController
            
            destination?.serviceTypeID = serviceTypeID
            destination?.serviceStartDate = serviceStartDate
            destination?.serviceEndDate = serviceEndDate
            destination?.contentSizeInPopup = CGSize(width: 300, height: 300)
             destination?.nurseAssigmentVC = self
            let popupController = STPopupController.init(rootViewController: destination!)
            popupController.containerView.layer.cornerRadius = 5.0
            popupController.present(in: self)
    }
    
    @objc func buttonBackNurse(sender:UIButton!){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nurseCreateInvoice(_ sender: Any) {
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        
        guard let  staffIDs = selectedNurseID  else {
            return
        }
        
        guard let serviceID = serviceID else {
            return
        }
        guard let serviceTypename = serviceTypeName else {
            return
        }
        
        guard let userID = userId else {
            return
        }

        if InternetConnection.connected(){
            self.createInvoiceTapped.animate(animation: .collapse)
            assignServiceToStaff(admin_id: adminID, staff_id: staffIDs, service_id: serviceID, service_type_name: serviceTypename, user_id: userID)
         }
        
        else{
            Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.ConnectionAlert, controller: self, okBtnTitle: Constants.OKTItle)
        }
    }

    
    
    func assignServiceToStaff (admin_id : Int,staff_id:String,service_id:String,service_type_name:String,user_id:String)
    {
        let parameters = ["admin_id":admin_id,"staff_id":selectedNurseID!,"service_id": serviceID,"service_type_name":serviceTypeName ,"user_id": userId] as [String:Any]
        AdminServices.assignServiceToStaff(Constants.AssignServiceApi, params: parameters, completion: { (response, error) in
            if let _ = error {
                self.createInvoiceTapped.animate(animation: .expand)
                // show alert with err message
                Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
            }else{
                // response
                if let result =  response as? AssignServiceModel {
                    if result.status!{
                        self.nurseRequestsViewController?.showAllNurseRequests()
                        self.navigationController?.popViewController(animated: true)
        
                    }else{
                        self.createInvoiceTapped.animate(animation: .expand)
                         Helper.showAlert(title: Constants.errorAlertTitle, message: Constants.FailureAlert, controller: self, okBtnTitle: Constants.OKTItle)
                    }
                }else{
                    return
                }
                
                
            }
        })
        

        
    }
    
    

}
