//
//  DatePopupPickerViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 4/3/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class DatePopupPickerViewController: UIViewController {
    
    var availableNurseVC : AvailableNurseViewController?
    var isStartDate:Bool?
    var submitStartDate:String?
    var submitEndDate:String?
    var selectDate:String?
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        var currentDate:Date = Date()
        var components:DateComponents = DateComponents()
        var calendar:Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        if isStartDate == true{
            var currentDate:Date = Date()
            var components:DateComponents = DateComponents()
            var calendar:Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            components.calendar = calendar
            //components.day = +1
            datePicker.minimumDate = calendar.date(byAdding: components, to: currentDate)
        }
        else{
            let minimumDate: Date = (availableNurseVC?.minimumEndDate)!
            var components: DateComponents = DateComponents()
            var calendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            components.calendar = calendar
            //components.hour = +1
            datePicker.minimumDate = calendar.date(byAdding: components, to: minimumDate)
        }
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
    }

    @objc func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        selectDate = dateFormatter.string(from: sender.date)
        if isStartDate == true{
            availableNurseVC?.minimumEndDate = sender.date
            let select1 = fullDateConverter(dateString: selectDate!)
            availableNurseVC?.selectedStartDateTime = String(describing: select1!)
        }else{
            let select1 = fullDateConverter(dateString: selectDate!)
            availableNurseVC?.selectedEndDateTime = String(describing: select1!)
        }
    }
    
     func fullDateConverter(dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // This formate is input formated .
        let formateDate = dateFormatter.date(from: dateString)!
        dateFormatter.dateFormat = "yyyy-MM-dd h:mm a" // Output Formated
        return dateFormatter.string(from: formateDate)
    }
    
    @IBAction func submitDateAction(_ sender: Any) {
        datePickerValueChanged(sender: datePicker)
        dismiss(animated: true, completion: nil)
    }
}
