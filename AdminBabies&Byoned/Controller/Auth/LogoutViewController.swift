//
//  LogoutViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 2/20/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
import SDWebImage

class LogoutViewController: UIViewController {
    // MARK:- Outlets
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var profilename: UILabel!
    @IBOutlet weak var profileEmail: UILabel!
    @IBOutlet weak var profileNumber: UILabel!
    @IBOutlet weak var logoutButton: UIButton!
    
    // MARK:- Variables
    var profileLoginData = AccountManager.shared().userData
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        profileImageView.layer.borderWidth = 1
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        logoutButton.layer.cornerRadius = 5.0
        showData()
        self.title = "Account"
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    @IBAction func LogoutTapped(_ sender: Any) {
        let alertController = UIAlertController(title: "Are you sure you want to Logout? ", message: "", preferredStyle: .alert)
        //create the actions
        let okButton = UIAlertAction(title: "Logout", style: UIAlertActionStyle.default) { UIAlertAction in
            self.logoOutRequest()
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel) {
            UIAlertAction in
        }
        // Add the actions
        alertController.addAction(okButton)
        alertController.addAction(cancelButton)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showData(){
        if let showData = profileLoginData{
            profilename.text = showData.name
            profileEmail.text = showData.email
            profileNumber.text = showData.phone
            profileImageView.sd_setImage(with: URL(string:(profileLoginData?.photo)!), placeholderImage: UIImage(named: "Personimage"))
        }
        else{
        }
    }
    
    func logoOutRequest(){
        guard let adminID = AccountManager.shared().userData?.id else{
            return
        }
        Helper.removeCachedUser()
        AccountManager.shared().clear()
        let parameters = ["user_id":adminID] as [String:Any]
        AdminServices.logoutRequest(Constants.logoutApi, params: parameters, completion: nil)
        self.backToLoginScreen()
    }
}
