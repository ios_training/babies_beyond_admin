//
//  AllClientsDetailsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 3/14/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class AllClientsDetailsViewController: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var clientName: UILabel!
    @IBOutlet weak var clientPhone: UILabel!
    @IBOutlet weak var clientEmail: UILabel!
    @IBOutlet weak var clientBirthDate: UILabel!
    
    var clientData : ClientData?

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        profileImageView.layer.borderWidth = 1
        profileImageView.layer.masksToBounds = false
        profileImageView.layer.borderColor = UIColor.white.cgColor
        profileImageView.layer.cornerRadius = profileImageView.frame.height/2
        profileImageView.clipsToBounds = true
        setupView()
        self.title = "Client Details"
        
    }
    func setupView(){
        if let clientObj = clientData{
            
            profileImageView.sd_setImage(with: URL(string:(clientObj.photo)!), placeholderImage: UIImage(named: "Personimage"))
            
            if let name = clientObj.name{
                clientName.text = name
            }
            if let phone = clientObj.phone{
                clientPhone.text = phone
            }
            if let email = clientObj.email{
                clientEmail.text = email
            }
            if let date = clientObj.birthday{
                clientBirthDate.text = Helper.dateConverterWithFormat(dateString : date, isDate: true)
            }else{
                clientBirthDate.text = "None"
            }

            
        }
        
    }


}
