//
//  ScheduleViewController.swift
//  Babies&Beyond
//
//  Created by esam ahmed eisa on 12/18/17.
//  Copyright © 2017 NTAM. All rights reserved.
//

import UIKit
import ISHPullUp
import STPopup


class ScheduleViewController: UIViewController ,ISHPullUpSizingDelegate, ISHPullUpStateDelegate, CalenadarViewControllerDelegate{

    
    var staffDataByDateArr = [StaffDataByDate]()
    
    @IBOutlet weak var nursetableview: UITableView!
    @IBOutlet weak var handleView: ISHPullUpHandleView!
    @IBOutlet weak var topview: UIView!
    @IBOutlet weak var viewoftableview: UIView!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    
    private var firstAppearanceCompleted = false
    weak var pullUpController: ISHPullUpViewController!
    // we allow the pullUp to snap to the half way point
    private var halfWayPoint = CGFloat(0)

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        firstAppearanceCompleted = true;
    }

     override func viewDidLoad(  ) {
        super.viewDidLoad()
        loading.isHidden = true

       self.view.backgroundColor = UIColor(red: 81.0/255.0, green: 88/255.0, blue: 106/255.0, alpha: 1.0)
       //to call cell on table view
       let nib = UINib.init(nibName: "NurseAllStaffTableViewCell", bundle: nil)
        self.nursetableview.register(nib, forCellReuseIdentifier: "NurseAllStaffTableViewCell")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTapGesture))
        topview.addGestureRecognizer(tapGesture)
        nursetableview?.tableFooterView = UIView()
        }
    @objc func handleTapGesture(gesture: UITapGestureRecognizer) {
        if pullUpController.isLocked {
            return
        }
        
        pullUpController.toggleState(animated: true)
    }

    // MARK: ISHPullUpSizingDelegate
    
    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, maximumHeightForBottomViewController bottomVC: UIViewController, maximumAvailableHeight: CGFloat) -> CGFloat {
       return self.viewoftableview.frame.height
    }
    
    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, minimumHeightForBottomViewController bottomVC: UIViewController) -> CGFloat {
          return self.viewoftableview.frame.height/2.5
    }
    
    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, targetHeightForBottomViewController bottomVC: UIViewController, fromCurrentHeight height: CGFloat) -> CGFloat {
        // if around 30pt of the half way point -> snap to it
        if abs(height - halfWayPoint) < 30 {
            return halfWayPoint
        }
        
        // default behaviour
        return height
    }
    
    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, update edgeInsets: UIEdgeInsets, forBottomViewController bottomVC: UIViewController) {
        // we update the scroll view's content inset
        // to properly support scrolling in the intermediate states
        nursetableview.contentInset = edgeInsets
    }

    
    func pullUpViewController(_ pullUpViewController: ISHPullUpViewController, didChangeTo state: ISHPullUpState) {
        //topLabel.text = textForState(state);
        handleView.setState(ISHPullUpHandleView.handleState(for: state), animated: firstAppearanceCompleted)
    }
    
    func showAllNursesData(staffDataByDate: [StaffDataByDate]) {
        staffDataByDateArr.removeAll()
        staffDataByDateArr = staffDataByDate
        loading.isHidden = true
        self.nursetableview.reloadData()
    }

    func showLoading(isShow: Bool) {
        loading.isHidden = false
        loading.startAnimating()
    }

}
extension ScheduleViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(staffDataByDateArr.count)
        return staffDataByDateArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         let cell = nursetableview.dequeueReusableCell(withIdentifier: "NurseAllStaffTableViewCell", for: indexPath) as! NurseAllStaffTableViewCell
        
        let obj = staffDataByDateArr[indexPath.row]
        cell.allStaffViewDetails.isHidden = true
        if obj.photo != nil {
            cell.allStaffImage.sd_setImage(with: URL(string: obj.photo!), placeholderImage: UIImage(named: "Personimage"))
        }
        if let name = obj.name{
            cell.allStaffName.text = name
        }
        if let email = obj.email{
            cell.allStaffReserved.text = email
        }

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 100.0//Choose your custom row height
    }
}

