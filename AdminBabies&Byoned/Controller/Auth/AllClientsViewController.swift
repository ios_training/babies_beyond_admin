//
//  AllClientsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 3/14/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit
//import PagingTableView
import Toast_Swift


class AllClientsViewController: UIViewController, AllClientsDelegate{
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier = "AllClientsTableViewCell"
    var clientData = [ClientData]()
    var RemianingClientData = [ClientData]()
    var connectionBtn : ConnectionButton?
    var loadingView:LoadingView?
    var isLoadingMore = false // flag
    
    let threshold:CGFloat = 60.0
    var lastContentOffset: CGFloat = 0
    
    fileprivate lazy var reachability: NetReachability = NetReachability(hostname: "www.apple.com")
    var page = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        let centerY = self.view.bounds.size.height/2
        let frame = CGRect(x: 0, y: centerY, width: self.view.bounds.size.width, height: 200)
        loadingView = LoadingView(frame: frame)
        self.view.addSubview(loadingView!)

        tableView.dataSource = self
        tableView.tableFooterView = UIView()

        let nib = UINib.init(nibName: "AllClientsTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier:cellIdentifier)

        let nibFooter = UINib.init(nibName: "LoadingFooterView", bundle: nil)
        self.tableView.tableFooterView = nibFooter.instantiate(withOwner: nil, options: nil)[0] as? UIView
        
        let framebtn = CGRect(x: 0, y:0, width: self.view.bounds.size.width, height: 200)
        connectionBtn = ConnectionButton(frame: framebtn)
        self.view.addSubview(connectionBtn!)
        connectionBtn?.addTarget(self, action: #selector(AllClientsViewController.connectionBtnTapped), for: .touchUpInside)
        tableView?.isHidden = true
        loadingView?.isHidden = true
        connectionBtn?.isHidden = true

    }
    @objc func connectionBtnTapped() {
       if InternetConnection.connected(){
            getClinets(page : page)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    override func viewDidLayoutSubviews() {
        connectionBtn?.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        connectionBtn?.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
    }

    func getClinets(page : Int) {
        let parameters = ["page":page] as [String:Any]
        if self.clientData.count == 0{
            self.loadingView?.isHidden = false
            connectionBtn?.isHidden = true
        }
        AdminServices.getAllClients(Constants.GetAllClientsApi, params: parameters, completion: { (response, error) in
             self.loadingView?.isHidden = true
            if let _ = error {
                self.connectionBtn?.isHidden = false
                self.connectionBtn?.setTitle("Something went wrong, press to try again.", for: .normal)
            }else{
                // response
                if let result =  response as? AllClientResponse {
                    if result.status!{
                        if let clientsData =  result.data {
                            self.RemianingClientData.removeAll()
                            self.RemianingClientData = clientsData

                            if self.RemianingClientData.count > 0{
                                self.clientData.append(contentsOf: self.RemianingClientData)
                                print(self.clientData.count)
                                self.tableView.reloadData()
                                self.connectionBtn?.isHidden = true
                                self.tableView?.isHidden = false
                                //self.isRefresh = false
                                self.page += 1
                                 self.isLoadingMore = false
                            }else{
                                self.tableView.tableFooterView = nil
                            }
                        }
                    }
                }else{
                    //Error
                }
            }
        })
        
    }
    
    func showAllClientsDetails(btnTag:Int) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AllClientsDetailsViewController") as! AllClientsDetailsViewController
        vc.clientData = clientData[btnTag]
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func addNewClient(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "AddNewClientViewController") as! AddNewClientViewController
        vc.allClientsViewController = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
extension AllClientsViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return clientData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)  as! AllClientsTableViewCell
        let obj = clientData[indexPath.row]
        guard clientData.indices.contains(indexPath.row) else { return cell }
        cell.AllClientsImage.sd_setImage(with: URL(string: obj.photo!), placeholderImage: UIImage(named: "Personimage"))
        cell.AllClientsName.text = obj.name
        cell.delegate = self
        cell.AllClientsDetailsButton.tag = indexPath.row
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        if !isLoadingMore && (maximumOffset - contentOffset <= threshold){
            // Get more data - API call
            self.isLoadingMore = true
            if InternetConnection.connected(){
                let nibFooter = UINib.init(nibName: "LoadingFooterView", bundle: nil)
                self.tableView.tableFooterView = nibFooter.instantiate(withOwner: nil, options: nil)[0] as? UIView
                getClinets(page : page)
            }else if reachability.currentReachabilityStatus == .notReachable{
                if clientData.count > 0 {
                    if (self.lastContentOffset < scrollView.contentOffset.y){
                        self.view.makeToast("Check your Internet Connection!", duration: 1.0, position: .bottom)
                    }
                }else{
                    connectionBtn?.isHidden = false
                }
                self.isLoadingMore = false
                self.tableView.tableFooterView = nil
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.lastContentOffset = scrollView.contentOffset.y
    }
}
