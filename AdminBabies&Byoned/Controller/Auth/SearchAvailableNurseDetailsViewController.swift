//
//  SearchAvailableNurseDetailsViewController.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 4/4/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import UIKit

class SearchAvailableNurseDetailsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    let cellIdentifier = "NurseAllStaffTableViewCell"
    var availableNursesSearch = [StaffDataByDate]()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.tableFooterView = UIView()
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 80
        let nib = UINib.init(nibName: "NurseAllStaffTableViewCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier:cellIdentifier)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = "Nurse Available"
        self.navigationItem.hidesBackButton = true
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "leftArrow icon"), style: UIBarButtonItemStyle.done, target: self, action: #selector(SearchAvailableNurseDetailsViewController.cancel(_:)))
    }
    
    @IBAction func cancel(_ sender:Any){
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
    
}



extension SearchAvailableNurseDetailsViewController : UITableViewDelegate, UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(availableNursesSearch)
        return availableNursesSearch.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)  as! NurseAllStaffTableViewCell
        let obj = availableNursesSearch[indexPath.row]
        cell.allStaffViewDetails.isHidden = true
        
        if let name = obj.name{
            cell.allStaffName.text = name
        }
        
        if let email = obj.email{
            cell.allStaffReserved.text = email
        }
        
        if obj.photo != nil {
            cell.allStaffImage.sd_setImage(with: URL(string : obj.photo!), placeholderImage: UIImage(named: "Personimage"))
        }
        return cell
    }
    
}

