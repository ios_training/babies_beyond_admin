//
//  AccountManager.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/14/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation

class AccountManager{
    //shared data
//    var loginResponse : LoginResponse?
    var userData: User_data?
    //nurse
    var nurseRequestRespone: NurseRequestResponse?
    
    //nurse and babysitter all staff
    var availableStaff : GetAvailableStaffResponse?
    
    //babysitter
    var babySitterRequestRespone: NurseRequestResponse?
    
    //workshop
    var workshopRespone: WorkshopRequestsResponse?
    var allStaffAdminWorkshops :AllStaffWorkshopsResponse?
    
  
    //search
    var availableNurse: StaffDataByDateResponse?
    
    //midwife
    //all staff
    var midWifesAllStaffResponse : MidWifesRequestResponse?
    //requests
    var midwifesReservedRequestResponse : MidwifesReservedResponse?
    
    var allClients : [ClientData]?
    
    private static var sharedInstance = AccountManager()
    static func shared()->AccountManager{
        return sharedInstance
    }
    
    func clear() {
        userData = nil
        nurseRequestRespone = nil
        babySitterRequestRespone = nil
        workshopRespone = nil
        allStaffAdminWorkshops = nil
        availableStaff = nil
        midWifesAllStaffResponse = nil
        midwifesReservedRequestResponse = nil
        allClients = nil
        availableNurse = nil
    }
}
