//
//  Constants.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/4/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
struct Constants {
    //IDs of service
    static let NurseTypeID = "1"
    static let BabysitterTypeID = "2"
    static let MidwifeTypeID = "3"
    
    //API Constants
    static let BASE_URL = "http://ntam.tech/Babies_And_Beyond/api/"
    static let LoginApi = "login"
    static let AllAdminNurseRequestApi = "staff_requests"
    static let AssignServiceApi = "assign_service_to_staff"
    static let AdminWorkShopRequests = "workshop_requests"
    static let AllListsAdminWorkshops = "admin_workshops"
    static let WorkShopInvoice = "create_workshop_invoice"
    static let AvailableStaff = "get_avaliable_staff"
    static let logoutApi = "logout"
    static let MidWifesAllStaffApi = "midwife/all"
    static let MidwifeReservationsApi = "midwife/get-reservations"
    static let MidwifeConfirmWithoutPaymentApi = "midwife/confirm-without-payment"
    static let GetAllClientsApi = "user/all-clients"
    static let CreateClientApi = "add_user"
    static let AvailableNursesApi = "calendar/available-nurses"
    static let OccupiedNursesApi = "calendar/occupied-nurses"

    
    
    // Alerts Constants:
    static let OKTItle  =  "Ok"
    static let errorAlertTitle = ""
    
    // 1- Internet connection messages in alerts
    static let ConnectionAlert  =  "Please Check Your Internet Connection!"
    static let FailureAlert  =  "Something went wrong, Please try again"
    static let FailureAlertiPhone5  =  "Something went wrong, \nPlease try again"

    static let EmptyName  =  "Please enter your name."
    static let EmptyPhoneNumber  =  "Please enter your phone number."
    static let PasswordsNotMatched  =  "Password Confirmation doesn't match the password ."

    
    
    static let SuccessAlert  =  "Successful Create Invoice"
    static let ReservationAlert = "Reservation confirmed successfully"
    
    // 2- Login messages in alerts
    static let EmptyEmail  =  "Please enter your email."
    static let InvalidEmail  =  "Your email is wrong."
    static let EmptyPassword  =  "Please enter your password."
    static let PasswordCharactersLessThan6Digits = "Password must be 6 digits or characters or more."
    static let NoAdmin = "Email or password is wrong"

    //3- choose nurse alert
    //4- choose babysitter alert
    static let NoStaffAvailable = "No staff available"
    
    //create cashing
    static let LoggedUserKey = "loggedUser"
    
    //cloud messages keys
    static let Id = "id"
    static let Action = "action"
    static let type = "type"
    static let workshopObj = "workshop_obj"
    static let serviceObj = "service_obj"
    
    //choose Start And End Date
    static let StartDateAlert  =  "Please Choose Start Date"
    static let EndDateAlert  =  "Please Choose End Date"
    static let SuccessSearchAlert  =  "Data Search successfully"



    
}
