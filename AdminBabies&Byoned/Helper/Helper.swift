//
//  Helper.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/14/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit

class Helper: NSObject {
    
    static func showAlert(title:String, message: String, controller : UIViewController, okBtnTitle:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: okBtnTitle, style: .default, handler: {(_ action: UIAlertAction) -> Void in
            // dismiss alert
        })
        alert.addAction(okButton)
        controller.present(alert, animated: true, completion: nil)

    }
    
    static func fullDateConverter(dateString: String) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // This formate is input formated .
        let formateDate = dateFormatter.date(from: dateString)!
        dateFormatter.dateFormat = "dd MMM yyyy h:mm a" // Output Formated
        return dateFormatter.string(from: formateDate)
    }
    
    static func dateConverterWithFormat(dateString: String, isDate: Bool) -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss" // This is date format which comes from the backend.
        let formateDate = dateFormatter.date(from: dateString)!
        // This is the new date format which we need
        if isDate{
            dateFormatter.dateFormat = "dd MMM yyyy"
        }else{
            dateFormatter.dateFormat = "h:mm a"
        }
        
        return dateFormatter.string(from: formateDate)
    }
    
    static func cacheUser(userData:User_data) {
        do {
            let data = try JSONEncoder.init().encode(userData)
            UserDefaults.standard.set(data, forKey: Constants.LoggedUserKey)
            UserDefaults.standard.synchronize()
        } catch  {
            // it's impossible to occur any error
        }
    }
    
    static func removeCachedUser() {
        UserDefaults.standard.removeObject(forKey: Constants.LoggedUserKey)
        UserDefaults.standard.synchronize()
    }
    
    static func getCachedUser() -> User_data? {
        do {
            if let data = UserDefaults.standard.value(forKey: Constants.LoggedUserKey) as? Data {
                let userDataObj = try JSONDecoder.init().decode(User_data.self, from: data)
                return userDataObj
            }
            return nil
        } catch  {
            return nil
        }
    }
}
