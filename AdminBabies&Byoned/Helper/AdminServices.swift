//
//  AdminServices.swift
//  AdminBabies&Byoned
//
//  Created by NTAM Tech on 1/18/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import Reachability


class AdminServices:NSObject{
    static let userToken = "Bearer \((AccountManager.shared().userData?.user_token)!)"
    static let notificationToken = defaults.string(forKey: "notificationToken")

    
    //Admin Login Service
    static func AdminLoginService(_ apiName: String, params: [String : Any]? ,completion: ((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:nil).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                
                    let adminLoginResponse = try jsonDecoder.decode(LoginResponse.self, from: response.data!)
                    completion?(adminLoginResponse, nil)
                } catch let jsonErr {
                    completion?(nil, jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
        
    }
    
    
    
    //Admin Staff Request Service
    static func allAdminNurseRequests(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let sendNurseRequestResponse = try jsonDecoder.decode(NurseRequestResponse.self, from: response.data!)
                    completion?(sendNurseRequestResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }

    
    //Admin Staff Assign Service(Create Invoice Button)
    static func assignServiceToStaff(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let assignServiceModel = try jsonDecoder.decode(AssignServiceModel.self, from: response.data!)
                    completion?(assignServiceModel, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                    
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    //Admin Workshop SendRequests
    static func AdminworkshopRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let workshopRespons = try jsonDecoder.decode(WorkshopRequestsResponse.self, from: response.data!)
                    completion?(workshopRespons, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    //Admin Workshop AllLists
    static func AllListWorkshop(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let adminWorkshopsResponse = try jsonDecoder.decode(AllStaffWorkshopsResponse.self, from: response.data!)
                    completion?(adminWorkshopsResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    //Admin workshop Assign Service(Create Invoice Button)
    static func createWorkshopInvoiceService(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let workshopinvoice = try jsonDecoder.decode(WorkshopCreateInvoiceResponse.self, from: response.data!)
                    completion?(workshopinvoice, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    //Admin Get Available Staff(drop down)
    static func getAvailableStaff(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let dropDownAvailableStaff = try jsonDecoder.decode(GetAvailableStaffResponse.self, from: response.data!)
                    completion?(dropDownAvailableStaff, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    //Admin Logout
    static func logoutRequest(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?){
        let urlString = Constants.BASE_URL + apiName;
        let url = URL(string: urlString);
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let logoutResponse = try jsonDecoder.decode(LogoutResponse.self, from: response.data!)
                    completion?(logoutResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    //Admin MidWifes SendRequests
    static func MidWifesAllStaff(_ apiName: String,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Authorization":userToken]
        Alamofire.request(url!, method:.get,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let midWifesResponse = try jsonDecoder.decode(MidWifesRequestResponse.self, from: response.data!)
                    completion?(midWifesResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    //Admin Get Requests Midwife Reservations
    static func getMidewifeReservations(_ apiName: String,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: nil,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let midWifesReservationResponse = try jsonDecoder.decode(MidwifesReservedResponse.self, from: response.data!)
                    completion?(midWifesReservationResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    //Admin confirmWithoutPayment service
    static func MidwifeConfirmWithoutPayment(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let AdminConfirmWithoutPayment = try jsonDecoder.decode(ConfirmWithoutPaymentResponse.self, from: response.data!)
                    completion?(AdminConfirmWithoutPayment, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    //GetAllClients
    static func getAllClients(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let allClientResponse = try jsonDecoder.decode(AllClientResponse.self, from: response.data!)
                    completion?(allClientResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    // Add User
    static func createClient(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Accept":"application/json","Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let createClientResponse = try jsonDecoder.decode(CreateClientResponse.self, from: response.data!)
                    completion?(createClientResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                    print(jsonErr.localizedDescription)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
    
    
    
    //Admin Staff Request Service
    static func GetAvailableNurses(_ apiName: String, params: [String : Any]? ,completion:((Any?, Error?) -> ())?) {
        let urlString = Constants.BASE_URL + apiName
        let url = URL(string: urlString)
        let headers = ["Authorization":userToken]
        
        Alamofire.request(url!, method:.post, parameters: params,encoding: JSONEncoding.default, headers:headers).responseJSON { response in
            switch response.result {
            case .success:
                let jsonDecoder = JSONDecoder()
                do {
                    let sendAvailaleNurseResponse = try jsonDecoder.decode(StaffDataByDateResponse.self, from: response.data!)
                    completion?(sendAvailaleNurseResponse, nil)
                } catch let jsonErr {
                    completion?(nil,jsonErr)
                }
            case .failure:
                completion?(nil, response.error)
            }
        }
    }
}


