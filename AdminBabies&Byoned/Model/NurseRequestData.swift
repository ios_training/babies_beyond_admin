
import Foundation
struct NurseRequestData : Codable {
	var all_staff : [All_staff]?
	var staff_request : [NurseStaff_request]?

	enum CodingKeys: String, CodingKey {

		case all_staff = "all_staff"
		case staff_request = "staff_request"
	}
}
