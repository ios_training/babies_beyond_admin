
import Foundation
struct ReservedTimeSlots : Codable {
    var startHour:String?
    var endHour:String?
    public var date:String?
    var dayName:String?

    
    enum CodingKeys: String, CodingKey {
        case startHour = "from"
        case endHour = "to"
        case dayName
        case date = "date"
    }
}
