
import Foundation
class StaffDataByDateResponse : Codable {
	var status : Bool?
	var message : String?
	var data : [StaffDataByDate]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}

}
