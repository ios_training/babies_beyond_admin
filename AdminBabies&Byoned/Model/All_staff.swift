
import Foundation
struct All_staff : Codable {
    let id : String?
    let name : String?
    let email : String?
    let password : String?
    let phone : String?
    let photo : String?
    let notification_token : String?
    let birthday : String?
    let user_type_id : String?
    let created_by : String?
    let is_logged_in : String?
    let remember_token : String?
    let created_at : String?
    let updated_at : String?
    let user_type_name : String?
    let is_reserved : Bool?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case email = "email"
        case password = "password"
        case phone = "phone"
        case photo = "photo"
        case notification_token = "notification_token"
        case birthday = "birthday"
        case user_type_id = "user_type_id"
        case created_by = "created_by"
        case is_logged_in = "is_logged_in"
        case remember_token = "remember_token"
        case created_at = "created_at"
        case updated_at = "updated_at"
        case user_type_name = "user_type_name"
        case is_reserved = "is_reserved"
    }

}
