
import Foundation
struct MidWifesReservedData : Codable {
	var name : String?
	var email : String?
	var phone : String?
	var photo : String?
	var created_by : String?
	var price_per_hour : String?
	var user_name : String?
	var user_photo : String?
	var id : String?
	var service_workshop_status_name : String?
	var time_slots : [ReservedTimeSlots]?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case email = "email"
		case phone = "phone"
		case photo = "photo"
		case created_by = "created_by"
		case price_per_hour = "price_per_hour"
		case user_name = "user_name"
		case user_photo = "user_photo"
		case id = "id"
		case service_workshop_status_name = "service_workshop_status_name"
		case time_slots = "time_slots"
	}
}
