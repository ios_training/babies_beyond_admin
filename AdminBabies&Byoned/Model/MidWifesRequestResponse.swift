
import Foundation
struct MidWifesRequestResponse : Codable {
	let status : Bool?
	let message : String?
	let data : [MidWifesData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}

}
