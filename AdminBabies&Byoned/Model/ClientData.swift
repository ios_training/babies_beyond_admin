

import Foundation
class ClientData : Codable {
	var id : Int?
	var name : String?
	var email : String?
	var phone : String?
	var bio : String?
	var photo : String?
	var notification_token : String?
	var notification_web : String?
	var birthday : String?
	var user_type_id : String?
	var created_by : String?
	var is_logged_in : String?
	var is_activate : String?
	var price_per_hour : String?
	var created_at : String?
	var updated_at : String?
	var verification_code : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case phone = "phone"
		case bio = "bio"
		case photo = "photo"
		case notification_token = "notification_token"
		case notification_web = "notification_web"
		case birthday = "birthday"
		case user_type_id = "user_type_id"
		case created_by = "created_by"
		case is_logged_in = "is_logged_in"
		case is_activate = "is_activate"
		case price_per_hour = "price_per_hour"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case verification_code = "verification_code"
	}
}
