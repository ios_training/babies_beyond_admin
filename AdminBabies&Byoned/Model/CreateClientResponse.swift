

import Foundation
class CreateClientResponse : Codable {
	var status : Bool?
	var message : String?
	var data : ClientData?

	enum CodingKeys: String, CodingKey {
		case status = "status"
		case message = "message"
		case data
	}
}
