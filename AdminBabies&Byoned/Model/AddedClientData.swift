
import Foundation
class AddedClientData : Codable {
	var name : String?
	var email : String?
	var phone : String?
	var photo : String?
	var birthday : String?
	var user_type_id : String?
	var is_activate : Int?
	var updated_at : String?
	var created_at : String?
	var id : Int?

	enum CodingKeys: String, CodingKey {

		case name = "name"
		case email = "email"
		case phone = "phone"
		case photo = "photo"
		case birthday = "birthday"
		case user_type_id = "user_type_id"
		case is_activate = "is_activate"
		case updated_at = "updated_at"
		case created_at = "created_at"
		case id = "id"
	}



}
