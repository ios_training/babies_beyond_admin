
import Foundation
struct LoginData : Codable {
	let user_data : User_data?
	let home_page : Home_page?

	enum CodingKeys: String, CodingKey {

		case user_data
		case home_page
	}
}
