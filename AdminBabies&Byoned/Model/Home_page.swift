
import Foundation
struct Home_page : Codable {
	let services : [Services]?
	let service_types : [Service_types]?

	enum CodingKeys: String, CodingKey {

		case services = "services"
		case service_types = "service_types"
	}
}
