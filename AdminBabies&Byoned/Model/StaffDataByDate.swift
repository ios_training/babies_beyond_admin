
import Foundation
class StaffDataByDate : Codable {
	var id : String?
	var name : String?
	var email : String?
	var photo : String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case name = "name"
		case email = "email"
		case photo = "photo"
	}
}
