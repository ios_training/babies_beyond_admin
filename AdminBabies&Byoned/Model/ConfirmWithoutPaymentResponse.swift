
import Foundation
struct ConfirmWithoutPaymentResponse : Codable {
    let status : Bool?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
}
}
