
import Foundation
struct GetAvailableStaffResponse : Codable {
	let status : Bool?
	let message : String?
	var data : [GetAvailableStaff]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
