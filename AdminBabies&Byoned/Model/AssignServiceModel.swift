//
//  AssignServiceModel.swift
//  AdminBabies&Byoned
//
//  Created by NTAM on 1/21/18.
//  Copyright © 2018 NTAM. All rights reserved.
//

import Foundation
struct AssignServiceModel : Codable {
    let status : Bool?
    let message : String?
    
    enum CodingKeys: String, CodingKey {
        
        case status = "status"
        case message = "message"
    }

    
}
