
import Foundation
struct MidWifesData : Codable {
    let id : Int?
    let name : String?
    let email : String?
    let phone : String?
    let photo : String?
    let price_per_hour : String?
    let midwife_times : [MidWifesTimes]?
    
    enum CodingKeys: String, CodingKey {
        
        case id = "id"
        case name = "name"
        case email = "email"
        case phone = "phone"
        case photo = "photo"
        case price_per_hour = "price_per_hour"
        case midwife_times = "midwife_times"
    }
}
