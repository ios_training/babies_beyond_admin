
import Foundation
struct WorkshopRequestsResponse : Codable {
	var status : Bool?
	var message : String?
	var data : [WorkshopData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
