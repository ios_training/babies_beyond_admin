
import Foundation
struct MidwifesReservedResponse : Codable {
	var status : Bool?
	var message : String?
	var data : [MidWifesReservedData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}

}
