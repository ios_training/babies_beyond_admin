
import Foundation
struct AllClientResponse : Codable {
	var status : Bool?
	var data : [ClientData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case data = "data"
	}
}
