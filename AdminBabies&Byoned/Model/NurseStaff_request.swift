

import Foundation
struct NurseStaff_request : Codable {
	let id : String?
	let user_id : String?
	let staff_id : String?
	let start_date : String?
	let end_date : String?
	let location : String?
	let price : String?
	let rate : String?
	let staff_comment : String?
	let point : String?
	let created_by : String?
	let service_workshop_status_id : String?
	let service_type_id : String?
	let created_at : String?
	let updated_at : String?
	let service_type_name : String?
    let user_name : String?
    let user_photo: String?

	enum CodingKeys: String, CodingKey {

		case id = "id"
		case user_id = "user_id"
		case staff_id = "staff_id"
		case start_date = "start_date"
		case end_date = "end_date"
		case location = "location"
		case price = "price"
		case rate = "rate"
		case staff_comment = "staff_comment"
		case point = "point"
		case created_by = "created_by"
		case service_workshop_status_id = "service_workshop_status_id"
		case service_type_id = "service_type_id"
		case created_at = "created_at"
		case updated_at = "updated_at"
		case service_type_name = "service_type_name"
        case user_name = "user_name"
        case user_photo = "user_photo"
	}

}
