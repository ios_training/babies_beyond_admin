
import Foundation
struct AllStaffWorkshopsResponse : Codable {
	let status : Bool?
	let message : String?
	let data : [AllStaffWorkshopsData]?

	enum CodingKeys: String, CodingKey {

		case status = "status"
		case message = "message"
		case data = "data"
	}
}
